﻿namespace InterviewAssignment.Dtos.Contracts;

public class Result { 
    public bool IsFailure { get; set; }

    public List<string> Errors { get; set; }

    public CourseDto? Response { get; set; }
}
