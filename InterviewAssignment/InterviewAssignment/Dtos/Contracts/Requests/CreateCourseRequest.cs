﻿namespace InterviewAssignment.Dtos.Contracts.Requests;

public class CreateCourseRequest
{
    public string Title { get; set; }

    public string Description { get; set; }

    public string Status { get; set; }

    public bool IsPremium { get; set; }
}
