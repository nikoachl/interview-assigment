﻿using InterviewAssignment.Database.Entities;
using InterviewAssignment.Dtos;
using InterviewAssignment.Dtos.Contracts;
using InterviewAssignment.Dtos.Contracts.Requests;
using InterviewAssignment.Repository;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace InterviewAssignment.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CourseController : ControllerBase
{
    private readonly ICourseRepository _courseRepository;

    public CourseController(ICourseRepository courseRepository)
    {
        _courseRepository = courseRepository;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<CourseDto>>> GetCourses()
    {
        var courses = await _courseRepository.GetAsync();
        return Ok(courses);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<CourseDto>> GetCourse(short id)
    {
        var result = await _courseRepository.GetByIdAsync(id);

        if (result.IsFailure)
            return BadRequest(result.Errors);

        return Ok(result.Response);
    }

    [HttpPost]
    public async Task<ActionResult<CourseDto>> CreateCourse(CreateCourseRequest request)
    {
        var result = await _courseRepository.AddAsync(request);

        if(result.IsFailure)
        {
            return BadRequest(result.Errors);
        }

        return Ok(result.Response);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateCourse(short id, UpdateCourseRequest request)
    {
        var result = await _courseRepository.UpdateAsync(id, request);

        if (result.IsFailure)
            return BadRequest(result.Errors);

        return Ok(result.Response);

    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteCourse(short id)
    {
        var result = await _courseRepository.DeleteAsync(id);

        if (result.IsFailure)
            return BadRequest(result.Errors);

        return NoContent();
    }
}
