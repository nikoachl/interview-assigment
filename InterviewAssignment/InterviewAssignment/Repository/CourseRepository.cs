﻿using InterviewAssignment.Database;
using InterviewAssignment.Database.Entities;
using InterviewAssignment.Dtos;
using InterviewAssignment.Dtos.Contracts;
using InterviewAssignment.Dtos.Contracts.Requests;
using Microsoft.EntityFrameworkCore;

namespace InterviewAssignment.Repository;

public sealed class CourseRepository : ICourseRepository
{
    private readonly DataStore _context;

    public CourseRepository(DataStore context)
    {
        _context = context;
    }

    public async Task<Result> AddAsync(CreateCourseRequest request)
    {
        var course = new Course
        {
            Description = request.Description,
            IsPremium = request.IsPremium,
            Status = request.Status,
            Title = request.Title,
            CreatedAt = DateTime.UtcNow,
        };

        

        if(course.Status != "Published" && course.Status != "Pending")
        {
            var result = new Result
            {
                IsFailure = true,
                Errors = new List<string>() { "Wrong Status Text " },
                
            };
            return result;
        }


         await _context.Courses.AddAsync(course);
         await _context.SaveChangesAsync();




        return new Result
        {
            IsFailure = false,
            Response =  
            new CourseDto 
            {
                Description= course.Description,
                Title=course.Title,
                IsPremium= course.IsPremium,
                Id= course.Id,
                Status = course.Status
            }
        };
    }

    public async Task<Result> DeleteAsync(short id)
    {
        var course = await _context.Courses.FirstOrDefaultAsync(x=> x.Id == id);
        if(course != null)
        {
             _context.Courses.Remove(course);
            course.DeletedAt = DateTime.UtcNow;
            await _context.SaveChangesAsync();
            return new Result { IsFailure = false };
        }
        return new Result { IsFailure = true };
    }

    public async Task<List<CourseDto>> GetAsync()
    {
       var list =  await _context.Courses.ToListAsync();
       
       var result = new List<CourseDto>();
        foreach(var course in list)
        {
            result.Add(new CourseDto
            {
                Id = course.Id,
                Description = course.Description,
                IsPremium = course.IsPremium,
                Status = course.Status,
                Title = course.Title,
            });
        }

        return result;
    }

    public async Task<Result> GetByIdAsync(short id)
    {
        var course = _context.Courses.FirstOrDefault(c => c.Id == id);

        if (course != null)
        {
            return new Result
            {
                IsFailure = false,
                Response = new CourseDto
                {
                    Id = course.Id,
                    Description = course.Description,
                    IsPremium = course.IsPremium,
                    Status = course.Status,
                    Title = course.Title
                }
            };
        }
        return new Result { IsFailure = true, Errors= new List<string>() { $"Course with id : {id} not found" } };
    }

    
    public async Task<Result> UpdateAsync(short id, UpdateCourseRequest item)
    {
        var course = await _context.Courses.FirstOrDefaultAsync(x => x.Id == id);
        if (course != null)
        {
            course.Title = item.Title;
            course.Description = item.Description;
            course.Status = item.Status;
            course.IsPremium = item.IsPremium;

            if (course.Status != "Published" && course.Status != "Pending")
            {
                var result = new Result
                {
                    IsFailure = true,
                    Errors = new List<string>() { "Wrong Status Text " },

                };
                return result;
            }

            _context.Courses.Update(course);
            await _context.SaveChangesAsync();

            return new Result
            {
                IsFailure = false,
                Response = new CourseDto
                {
                    Id = course.Id,
                    Description = course.Description,
                    IsPremium = course.IsPremium,
                    Status = course.Status,
                    Title = course.Title
                }
            };


        }
        return new Result { IsFailure = true, Errors = new List<string>() { $"Course with id : {id} not found" } };
    }

   
}

