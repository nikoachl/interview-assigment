﻿using InterviewAssignment.Dtos;
using InterviewAssignment.Dtos.Contracts;
using InterviewAssignment.Dtos.Contracts.Requests;

namespace InterviewAssignment.Repository;

public interface ICourseRepository : IEditRepository<short, CourseDto, Result>
{
}
