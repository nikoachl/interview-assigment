﻿using InterviewAssignment.Dtos;
using InterviewAssignment.Dtos.Contracts.Requests;

namespace InterviewAssignment.Repository;

public interface IEditRepository<Tkey,TDto,TRes>
{
    Task<List<TDto>> GetAsync();

    Task<TRes> GetByIdAsync(Tkey id);

    Task<TRes> AddAsync(CreateCourseRequest item);

    Task<TRes> UpdateAsync(Tkey id ,UpdateCourseRequest item);

    Task<TRes> DeleteAsync(Tkey id);
}
