using InterviewAssignment.Database;
using InterviewAssignment.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Register DataStore
builder.Services.AddDbContext<DataStore>(options => options.UseSqlite("Data Source=interviewAssigment.db"));

builder.Services.AddScoped<ICourseRepository,CourseRepository>();

var app = builder.Build();


if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
