﻿using InterviewAssignment.Database.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace InterviewAssignment.Database;

public  class DataStore : DbContext
{
    public DataStore(DbContextOptions options) : base(options){}

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlite();

    public DbSet<Course> Courses { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Course>(e =>
        {
            e.ToTable(nameof(Course));
            e.HasKey(x => x.Id);
            e.Property(x=> x.Id).ValueGeneratedOnAdd();
            e.Property(x => x.Description).HasMaxLength(150);
            e.Property(x => x.Title).HasMaxLength(50);
            e.Property(x=> x.CreatedAt).HasMaxLength(70);
            e.Property(x => x.DeletedAt).HasMaxLength(70);
        });

    }
}
