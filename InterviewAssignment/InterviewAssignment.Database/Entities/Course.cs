﻿namespace InterviewAssignment.Database.Entities;

public sealed class Course : BaseEntity
{
    public short Id { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public string Status { get; set; }

    public bool IsPremium { get; set; }

    
}
